import 'package:flutter/material.dart';
import 'package:japanese_scanner/screen/camera_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('日本語OCRアプリ'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (_) => CameraScreen(),
                  ),
                );
              },
              child: Text(
                '読み取り開始',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
